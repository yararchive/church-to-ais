-- ID ������, � ������� ��������� ������������ ��
DECLARE @archID INT
SET @archID = 4

-- ��� ������������ ���� ������
DECLARE @dbName NVARCHAR(2000)
SET @dbName = '��������� �������� ��������� 1917 �.'

DECLARE @dbID TABLE(Id INT)

-- �������� ������������ ���� ������ (���).
-- ����� �������� ID ��������� ���
INSERT INTO [dbo].[tblThemeCatalog] (
 ParentId
 ,Name
 ,CreationTime
 ,DeletionTime
 ,Deleted
 ,ArchiveId
)
OUTPUT Inserted.Id INTO @dbID
SELECT 0 ,@dbName ,CURRENT_TIMESTAMP ,NULL ,0 ,@archID
 
-- ������� ����� ��
INSERT INTO [dbo].[tblItemsAdditionalFields] (
 ItemType
 ,ParentItemId
 ,FieldType
 ,ColumnName
 ,ShownName
 ,OrderNumber
 ,ItemType2
 ,CustomDictionaryId
 ,DictionaryId
 ,ArchiveId
)
VALUES
  (9, (SELECT Id FROM @dbID), 1, 'nvarchar9', '�����', 1, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 6, 'bit4', '��������� (�����������) ���������', 2, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 6, 'bit5', '������������ ��������', 3, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar28', '��� �������������', 4, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 2, 'ntext1', '���������� ������', 5, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar31', '��� ���������������', 6, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 2, 'ntext2', '������ ���, ������� � ��������', 7, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar32', '����������', 8, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar1', '��� ����������� ������', 9, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar2', '���������� �����', 10, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar30', '������ (�����, ��������, �������), ���, ��������', 11, NULL, NULL, NULL, @archID)
 
