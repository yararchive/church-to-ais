-- ID ������, � ������� ��������� ������������ ��
DECLARE @archID INT
SET @archID = 1

-- ��� ������������ ���� �����
DECLARE @dbName NVARCHAR(2000)
SET @dbName = '��������� ������'

DECLARE @dbID TABLE(Id INT)

-- �������� ������������ ���� ������ (���).
-- ����� �������� ID ��������� ���
INSERT INTO [dbo].[tblThemeCatalog] (
 ParentId
 ,Name
 ,CreationTime
 ,DeletionTime
 ,Deleted
 ,ArchiveId
)
OUTPUT Inserted.Id INTO @dbID
SELECT 0 ,@dbName ,CURRENT_TIMESTAMP ,NULL ,0 ,@archID
 
-- ������� ����� ��
INSERT INTO [dbo].[tblItemsAdditionalFields] (
 ItemType
 ,ParentItemId
 ,FieldType
 ,ColumnName
 ,ShownName
 ,OrderNumber
 ,ItemType2
 ,CustomDictionaryId
 ,DictionaryId
 ,ArchiveId
)
VALUES
 (9, (SELECT Id FROM @dbID), 1, 'nvarchar18', '� �������', 1, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar8', '���', 2, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar1', '��� ����������� ������', 3, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar2', '���������� �����', 4, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar11', '�������', 5, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar13', '����', 6, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar3', '���� (�����)', 7, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar14', '���������', 8, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar15', '�������� (��������������)', 9, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar16', '��������', 10, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar17', '��������', 11, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar9', '�����', 12, NULL, NULL, NULL, @archID)
 
