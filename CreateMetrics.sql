-- ID ������, � ������� ��������� ������������ ��
DECLARE @archID INT
SET @archID = 2

-- ��� ������������ ���� �����
DECLARE @dbName NVARCHAR(2000)
SET @dbName = '����������� �����'

DECLARE @dbID TABLE(Id INT)

-- �������� ������������ ���� ������ (���).
-- ����� �������� ID ��������� ���
INSERT INTO [dbo].[tblThemeCatalog] (
 ParentId
 ,Name
 ,CreationTime
 ,DeletionTime
 ,Deleted
 ,ArchiveId
)
OUTPUT Inserted.Id INTO @dbID
SELECT 0 ,@dbName ,CURRENT_TIMESTAMP ,NULL ,0 ,@archID
 
-- ������� ����� ��
INSERT INTO [dbo].[tblItemsAdditionalFields] (
 ItemType
 ,ParentItemId
 ,FieldType
 ,ColumnName
 ,ShownName
 ,OrderNumber
 ,ItemType2
 ,CustomDictionaryId
 ,DictionaryId
 ,ArchiveId
)
VALUES
  (9, (SELECT Id FROM @dbID), 1, 'nvarchar1', '��� ���������� ������', 1, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar2', '��������� �����', 2, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar3', '����', 3, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar4', '��������: �����', 4, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar5', '�������: �����',	5, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar6', '������: �����', 6, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar7', '����', 7, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar8', '���', 8, NULL, NULL, NULL, @archID)
 
