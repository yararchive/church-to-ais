-- ID ������, � ������� ��������� ������������ ��
DECLARE @archID INT
SET @archID = 2

-- ��� ������������ ���� ������
DECLARE @dbNameMain NVARCHAR(2000)
SET @dbNameMain = '��������� ��������������'

DECLARE @dbIDMain TABLE(Id INT)

-- �������� ������������ ���� ������ ������� ������.
-- ����� �������� ID ��������� ���
INSERT INTO [dbo].[tblThemeCatalog] (
 ParentId
 ,Name
 ,CreationTime
 ,DeletionTime
 ,Deleted
 ,ArchiveId
 ,IsHidden
 ,IsThematicCardsImagesHidden
)
OUTPUT Inserted.Id INTO @dbIDMain
SELECT 0 ,@dbNameMain ,CURRENT_TIMESTAMP ,NULL ,0 ,@archID, 0, 0
 
-- ������� �������������� ����� �� ������� ������
INSERT INTO [dbo].[tblItemsAdditionalFields] (
 ItemType
 ,ParentItemId
 ,FieldType
 ,ColumnName
 ,ShownName
 ,OrderNumber
 ,ItemType2
 ,CustomDictionaryId
 ,DictionaryId
 ,ArchiveId
)
VALUES
  (9, (SELECT Id FROM @dbIDMain), 1, 'nvarchar40', '���� ������', 1, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbIDMain), 1, 'nvarchar41', '����� ������', 2, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbIDMain), 1, 'nvarchar42', '����� ������', 3, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbIDMain), 1, 'nvarchar43', '����������', 4, NULL, NULL, NULL, @archID)
 
 
