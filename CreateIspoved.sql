-- ID ������, � ������� ��������� ������������ ��
DECLARE @archID INT
SET @archID = 2

-- ��� ������������ ���� �����
DECLARE @dbName NVARCHAR(2000)
SET @dbName = '���������� �������'

DECLARE @dbID TABLE(Id INT)

-- �������� ������������ ���� ������ (���).
-- ����� �������� ID ��������� ���
INSERT INTO [dbo].[tblThemeCatalog] (
 ParentId
 ,Name
 ,CreationTime
 ,DeletionTime
 ,Deleted
 ,ArchiveId
)
OUTPUT Inserted.Id INTO @dbID
SELECT 0 ,@dbName ,CURRENT_TIMESTAMP ,NULL ,0 ,@archID
 
-- ������� ����� ��
INSERT INTO [dbo].[tblItemsAdditionalFields] (
 ItemType
 ,ParentItemId
 ,FieldType
 ,ColumnName
 ,ShownName
 ,OrderNumber
 ,ItemType2
 ,CustomDictionaryId
 ,DictionaryId
 ,ArchiveId
)
VALUES 
  (9, (SELECT Id FROM @dbID), 1, 'nvarchar9', '�����', 1, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar10', '���������', 2, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar7', '����', 3, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar2', '��������� �����', 4, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar3', '����', 5, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar8', '���', 6, NULL, NULL, NULL, @archID)
 
