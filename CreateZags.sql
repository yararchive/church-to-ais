-- ID ������, � ������� ��������� ������������ ��
DECLARE @archID INT
SET @archID = 4

-- ��� ������������ ���� �����
DECLARE @dbName NVARCHAR(2000)
SET @dbName = '������� ������ ������������ ���������'

DECLARE @dbID TABLE(Id INT)

-- �������� ������������ ���� ������ (���).
-- ����� �������� ID ��������� ���
INSERT INTO [dbo].[tblThemeCatalog] (
 ParentId
 ,Name
 ,CreationTime
 ,DeletionTime
 ,Deleted
 ,ArchiveId
)
OUTPUT Inserted.Id INTO @dbID
SELECT 0 ,@dbName ,CURRENT_TIMESTAMP ,NULL ,0 ,@archID
 
-- ������� ����� ��
INSERT INTO [dbo].[tblItemsAdditionalFields] (
 ItemType
 ,ParentItemId
 ,FieldType
 ,ColumnName
 ,ShownName
 ,OrderNumber
 ,ItemType2
 ,CustomDictionaryId
 ,DictionaryId
 ,ArchiveId
)
VALUES
  (9, (SELECT Id FROM @dbID), 1, 'nvarchar9', '�����', 1, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar19', '����', 2, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar20', '�������', 3, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar21', '���', 4, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar22', '��������', 5, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 6, 'bit1', '��������', 6, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 6, 'bit2', '����', 7, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 6, 'bit3', '������', 8, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar1', '��� ����������� ������', 9, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar2', '���������� �����', 10, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar11', '�������', 11, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar3', '����', 12, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar23', '��������', 13, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar24', '�����', 14, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar25', '�������', 15, NULL, NULL, NULL, @archID)
 ,(9, (SELECT Id FROM @dbID), 1, 'nvarchar26', '����� �����������', 16, NULL, NULL, NULL, @archID)
 
