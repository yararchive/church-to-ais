SET IDENTITY_INSERT [AISArchive].[dbo].[tblThemeCards] ON

/*
 *
 * ID тематической БД метрических книг
 *
 */
DECLARE @CatId INT
SET @CatId = 13

/*
 *
 * ID архивного учреждения, в БД которого заливаются данные
 *
 */
DECLARE @ArchId INT
SET @ArchId = 2

DECLARE @whole_results TABLE(
 Id INT
 ,ItemId INT
 ,ThemeCatalogId INT
 ,Content VARCHAR(740)
 ,FundNumber NVARCHAR(255)
 ,InventoryNumber NVARCHAR(255)
 ,ArchiveFileNumber NVARCHAR(255)
 ,CreationTime DATETIME
 ,DeletionTime DATETIME
 ,Deleted BIT
 ,ArchiveId INT
 ,SortCode VARCHAR(160)
 ,nvarchar1 NVARCHAR(255)
 ,nvarchar2 NVARCHAR(255)
 ,nvarchar3 NVARCHAR(255)
 ,nvarchar11 NVARCHAR(255)
 ,nvarchar12 NVARCHAR(255)
 ,nvarchar13 NVARCHAR(255)
)

/*
 * Сбор данных в одну кучу во временную таблицу.
 * Потом раскидается по нужным таблицам
 *
 */
INSERT INTO @whole_results (
 Id
 ,ItemId
 ,ThemeCatalogId
 ,Content
 ,FundNumber
 ,InventoryNumber
 ,ArchiveFileNumber
 ,CreationTime
 ,DeletionTime
 ,Deleted
 ,ArchiveId
 ,SortCode
 ,nvarchar1
 ,nvarchar2
 ,nvarchar3
 ,nvarchar11
 ,nvarchar12
 ,nvarchar13
)
SELECT
 (IDENT_CURRENT('AISArchive.dbo.tblThemeCards') + ROW_NUMBER() OVER(ORDER BY Delo)) as Id,
 (IDENT_CURRENT('AISArchive.dbo.tblThemeCards') + ROW_NUMBER() OVER(ORDER BY Delo)) as ItemId,
 @CatId as ThemeCatalogId,
 ('Н/п: '
  + (CASE WHEN ISNULL(NULLIF(pt.[PoTypeName], '***'), '') NOT LIKE '' THEN LEFT(ISNULL(NULLIF(pt.[PoTypeName], '***'), ''), 1) + '. ' ELSE '' END)--+ LEFT(ISNULL(NULLIF(pt.[PoTypeName], '***'), ''), 1)
  + ISNULL(NULLIF(p.[PointName], '***'), '-')
  + (CASE WHEN ISNULL(pt.[PoTypeName], '') NOT LIKE 'город' THEN ' У: ' + ISNULL(NULLIF(u.[UezdName], '***'), '-') + '.' ELSE '' END)
  + (CASE WHEN ISNULL(pt.[PoTypeName], '') NOT LIKE 'город' THEN ' Вол: ' + ISNULL(NULLIF(vol.[VolostName], '***'), '-') + '.' ELSE '' END)
  + (CASE WHEN ISNULL(pt.[PoTypeName], '') LIKE 'город' THEN ' Ул: ' + ISNULL(NULLIF(adm.[AdmEdName], '***'), '-') + '.' ELSE '' END)
  + ' Заведение: ' + ISNULL(NULLIF(ob.[ObjectName], '***'), '-')
  ) as Content,
 '642' as FundNumber,
 '3' as InventoryNumber,
 ch.[Delo] as ArchiveFileNumber,
 CURRENT_TIMESTAMP as CreationTime,
 NULL as DeletionTime,
 0 as Deleted,
 @ArchId as ArchiveId,
 ('ф.' + RIGHT('000000000000000000000000000000000000000000000000' + CONVERT(VARCHAR, 642), 50)
  + ' оп.' + RIGHT('000000000000000000000000000000000000000000000000' + CONVERT(VARCHAR, 3), 50)
  + ' д.' + RIGHT('000000000000000000000000000000000000000000000000' + CONVERT(VARCHAR, ch.[Delo]), 50)) as SortCode,
 (ISNULL(NULLIF(pt.[PoTypeName], '***'), '-')) as nvarchar1,
 (ISNULL(NULLIF(p.[PointName], '***'), '-')) as nvarchar2,
 (ISNULL(NULLIF(u.[UezdName], '***'), '-')) as nvarchar3,
 (ISNULL(NULLIF(vol.[VolostName], '***'), '-')) as nvarchar11,
 (ISNULL(NULLIF(adm.[AdmEdName], '***'), '-')) as nvarchar12,
 (ISNULL(NULLIF(ob.[ObjectName], '***'), '-')) as nvarchar13
FROM
 [Church].[dbo].[Data] ch
LEFT JOIN [Church].[dbo].[PointsPer] p ON
 p.[PointID] = ch.[PointID]
LEFT JOIN [Church].[dbo].[Volost] vol ON
 vol.[VolostID] = ch.[VolostID]
LEFT JOIN [Church].[dbo].[PointsType] pt ON
 pt.[PoTypeID] = p.[PointType]
LEFT JOIN [Church].[dbo].[Uezd] u ON
 u.[UezdID] = ch.[UezdID]
LEFT JOIN [Church].[dbo].[AdmEd] adm ON
 adm.[AdmEdID] = ch.[AdmEdID]
LEFT JOIN [Church].[dbo].[Objects] ob ON
 ob.[ObjectID] = ch.[ObjectID]
WHERE
 ch.[status] = 1
 AND NOT EXISTS (
  SELECT *
  FROM
   [AISArchive].[dbo].[tblThemeCards] tc
  WHERE
   tc.FundNumber = 642
   AND tc.InventoryNumber = 3
   AND tc.ArchiveFileNumber = ch.Delo
   AND tc.ThemeCatalogId = @CatId
   AND tc.Deleted = 0
   AND tc.ArchiveId = @ArchId
   AND tc.Content =
    ('Н/п: '
      + (CASE WHEN ISNULL(NULLIF(pt.[PoTypeName], '***'), '') NOT LIKE '' THEN LEFT(ISNULL(NULLIF(pt.[PoTypeName], '***'), ''), 1) + '. ' ELSE '' END)--+ LEFT(ISNULL(NULLIF(pt.[PoTypeName], '***'), ''), 1)
      + ISNULL(NULLIF(p.[PointName], '***'), '-')
      + (CASE WHEN ISNULL(pt.[PoTypeName], '') NOT LIKE 'город' THEN ' У: ' + ISNULL(NULLIF(u.[UezdName], '***'), '-') + '.' ELSE '' END)
      + (CASE WHEN ISNULL(pt.[PoTypeName], '') NOT LIKE 'город' THEN ' Вол: ' + ISNULL(NULLIF(vol.[VolostName], '***'), '-') + '.' ELSE '' END)
      + (CASE WHEN ISNULL(pt.[PoTypeName], '') LIKE 'город' THEN ' Ул: ' + ISNULL(NULLIF(adm.[AdmEdName], '***'), '-') + '.' ELSE '' END)
      + ' Заведение: ' + ISNULL(NULLIF(ob.[ObjectName], '***'), '-')
    )
 )
ORDER BY
  pt.[PoTypeName] ASC,
  p.[PointName] ASC,
  u.[UezdName] ASC

/*
 * 
 * Вставить тематические карточки
 *
 */  
INSERT INTO [AISArchive].[dbo].[tblThemeCards] (
 Id
 ,ThemeCatalogId
 ,Content
 ,FundNumber
 ,InventoryNumber
 ,ArchiveFileNumber
 ,CreationTime
 ,DeletionTime
 ,Deleted
 ,ArchiveId
 ,SortCode
)
 SELECT
  Id
  ,ThemeCatalogId
  ,Content
  ,FundNumber
  ,InventoryNumber
  ,ArchiveFileNumber
  ,CreationTime
  ,DeletionTime
  ,Deleted
  ,ArchiveId
  ,SortCode
  FROM @whole_results

/*
 *
 * Вставить дополнительную информацию по тематическим карточкам  
 *
 */
INSERT INTO [AISArchive].[dbo].[tblItemsAdditionalFieldsValues] (
 ItemId
 ,ItemType
 ,nvarchar1
 ,nvarchar2
 ,nvarchar3
 ,nvarchar11
 ,nvarchar12
 ,nvarchar13
)
 SELECT
  ItemId
  ,9 as ItemType
  ,nvarchar1
  ,nvarchar2
  ,nvarchar3
  ,nvarchar11
  ,nvarchar12
  ,nvarchar13
  FROM @whole_results