/****** Сценарий для команды SelectTopNRows среды SSMS  ******/
SELECT
  aisa.ShortName AS 'Архив',
  ISNULL(NULLIF(ISNULL(aisf.Letter1, '') + '-', '-'), '') + CONVERT(nvarchar, aisf.Number) + ISNULL(aisf.Letter2, '') AS 'Номер фонда',
  ISNULL(aisf.Name, '') AS 'Наименование фонда',
  CONVERT(nvarchar, aisi.Number) + ISNULL(aisi.Letter, '') AS 'Номер описи',
  ISNULL(aisi.Name, '') AS 'Наименование описи',
  CONVERT(nvarchar, aisu.Number) + ISNULL(aisu.Letter, '') AS 'Номер дела',
  ISNULL(aisu.Name, '') AS 'Наименование дела'
FROM [AISArchive].[dbo].[tblArchiveFiles] aisu
INNER JOIN [AISArchive].[dbo].[tblInventories] aisi ON
  aisu.InventoryId = aisi.Id
INNER JOIN [AISArchive].[dbo].[tblFunds] aisf ON
  aisi.FundId = aisf.Id
INNER JOIN [AISArchive].[dbo].[tblArchives] aisa ON
  aisf.ArchiveId = aisa.Id
WHERE
  aisi.IsHidden = 0
  AND aisu.IsHidden = 0
  AND aisu.Deleted = 0
  AND aisi.Deleted = 0
  AND (SELECT COUNT(*) FROM [AISArchive].[dbo].[tblStorage] WHERE ItemType = 5 AND ItemId = aisu.Id) > 0
ORDER BY
  aisa.ShortName,
  aisf.Letter1,
  CONVERT(int, aisf.Number),
  aisf.Letter2,
  CONVERT(int, aisi.Number),
  aisi.Letter,
  CONVERT(int, aisu.Number),
  aisu.Letter